<?php

require_once './src/UploadFile.php';

use Chenjichun\UploadFile;

$config = [
    'field'     => 'file',
    'savePath'  => './' . date('Ymd'),
    'resizePer' => 0.5
];

echo '<pre>';

$upload = new UploadFile($config);

print_r($upload->upload($_FILES));